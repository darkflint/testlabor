{foreach from=$articles item=article}
	{block name="t2resArticleSlide"}
		{assign var=image value=$article.image.src.4 }
		<li>
	        <div class="articleSliderBox">
	        	<div class="innerSlideBox">
		        	{block name="t2resArticleSlideBoxInner"}
			            {if $image}
			            	<a class="thumbnail" title="{$article.articleName}" href="{$article.linkDetails}" style="background-image: url({$image});"></a>
			            {else}
			            	<a class="article-thumb-wrapper" title="{$article.articleName}" href="{$article.linkDetails}" style="background-image:url({link file='frontend/_resources/images/no_picture.jpg'});"></a>
			            {/if}
			            <a title="{$article.articleName}" class="title" href="{$article.linkDetails}">{$article.articleName|truncate:35}</a>
			
						{if $article.pseudoprice} 
							<span class="pseudoPrice">{s name="reducedPrice"}Statt: {/s}{$article.pseudoprice|currency} {s name="Star"}*{/s}</span>
						{/if}
			            
			            <span class="articlePrice">
			            	{if $article.priceStartingFrom && !$article.liveshoppingData}
			            		{s namespace="frontend/plugins/recommendation/slide_articles" name='ListingBoxArticleStartsAt'}{/s} 
			            	{/if}
			            	{$article.price|currency} *
			            </span>
		            {/block}
	        	</div>
	        </div>
		</li>
	{/block}
{/foreach}