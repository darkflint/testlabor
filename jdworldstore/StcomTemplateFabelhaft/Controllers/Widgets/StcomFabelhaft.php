<?php
/**
 * Fabelhaft Widget Controller
 * Copyright © 2014 twenty2media GmbH 
 *
 * @category   Shopware
 * @package    Shopware_Plugins
 * @subpackage Plugin
 * @copyright  Copyright (c) 2014, twenty2 media GmbH ( http://twenty2.de )
 * @version    $Id$
 * @author     twenty2
 */

/**
 * TwentyEasyLogin Widgetcontroller. Checks if user is logged in and adds the loginBox Template
 *
 * @category Shopware
 * @package Shopware\Plugin\TwentyEasyLogin
 * @copyright Copyright (c) 2014, twenty2media GmbH (http://www.twenty2.de)
 */
class Shopware_Controllers_Widgets_StcomFabelhaft extends Enlight_Controller_Action {
	protected $basket;
	
	/**
     * Reference to Shopware session object (Shopware()->Session)
     *
     * @var Zend_Session_Namespace
     */
    protected $session;
	
	/**
	 * Inits all variables of this controller
     * @public
     * @return void
	 */	
	public function init() {
		$this->basket = Shopware()->Modules()->Basket();
		$this->session = Shopware()->Session();
	}
	
	/**
	 * Login window action handler
     * @public
     * @return void
	 */
	public function testAction() {
		$userInfo = Shopware()->Modules()->Admin()->sGetUserData();
		die(json_encode($userInfo));
		return "HI";
	}
	/**
	 * returns amount of basket
     * @public
     * @return void
	 */	
    public function ajaxAmountAction() {
        $amount = $this->basket->sGetAmount();
		die(json_encode($amount));
	}
	/**
	 * mobile basket contents via ajax
     * @public
     * @return void
	 */	
	public function mobileBasketAction() {
		$view = $this->View();
        $amount = $this->basket->sGetAmount();
	 	$view->sBasketQuantity = isset($this->session->sBasketQuantity) ? $this->session->sBasketQuantity : 0;
        $view->sBasketAmount = isset($this->session->sBasketAmount) ? $this->session->sBasketAmount : 0;


	}
}
