{extends file='parent:frontend/custom/index.tpl'}

{block name='frontend_index_content'}


    <div id="center" class="custom grid_13">
    {if $sCustomPage.subPages}
        {$pages = $sCustomPage.subPages}
    {elseif $sCustomPage.siblingPages}
        {$pages = $sCustomPage.siblingPages}
    {/if}
    {if $pages}
        <div class="custom_subnavi">
            {if $pages}
                <ul class="sub-pages">
                {foreach $pages as $subPage}
                    <li><a href="{url controller=custom sCustom=$subPage.id}" title="{$subPage.description}" {if $subPage.active} class="active"{/if}>
                        {$subPage.description}
                    </a></li>
                {/foreach}
                </ul>
            {/if}
        </div>
    {/if}
    <br style="clear:both">
    
        <h1>{$sCustomPage.description}</h1>

        {* Article content *}
        {block name='frontend_custom_article_content'}
            {$sContent}
        {/block}
    </div>
{/block}
