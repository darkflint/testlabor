{extends file='parent:frontend/listing/listing.tpl'}

{block name="frontend_listing_top_actions"}
{if !$sOffers}
	{include file='frontend/listing/listing_actions_top.tpl' sTemplate=$sTemplate}
{/if}
{/block}


{* Paging *}
{block name="frontend_listing_bottom_paging"}
	{if !$sOffers}
			{include file='frontend/listing/listing_actions_bottom.tpl' sTemplate=$sTemplate}
	{else}
		{if $sCategoryContent.parent != 1}
		<div class="actions_offer">
			{include file='frontend/listing/listing_actions_bottom.tpl' sTemplate=$sTemplate}
		</div>
		{/if}
	{/if}
{/block}

