{if !$sOffers}

{block name="frontend_listing_actions_class"}
<div class="listing_actions{if !$sPages || $sPages.numbers|@count <= 1 || $sNumberPages <= 1} normal{/if} bottompos">
{/block}
{block name='frontend_listing_actions_paging'}
	{if $sNumberPages && $sNumberPages > 1}
	<div class="bottom">

		{* Paging *}
		<div class="paging">
			<label>{s name='ListingPaging' namespace='frontend/listing/listing_actions'}{/s}</label>
			{if $sPages.previous}
				<a href="{$sPages.previous|rewrite:$sCategoryInfo.name}" title="{s name='ListingLinkPrevious'  namespace='frontend/listing/listing_actions' }{/s}" class="navi prev"><i class="fa fa-angle-left"></i></a>
			{/if}

			{* Articles per page *}
			{foreach from=$sPages.numbers item=page}
				{if $page.value<$sPage+4 AND $page.value>$sPage-4}
					{if $page.markup AND (!$sOffers OR $sPage)}
						<a title="{$sCategoryInfo.name}" class="navi on">{$page.value}</a>
					{else}
						<a href="{$page.link|rewrite:$sCategoryInfo.name}" class="navi">{$page.value}</a>
					{/if}
				{elseif $page.value==$sPage+4 OR $page.value==$sPage-4}
					<div class="more">...</div>
				{/if}
			{/foreach}
			{if $sPages.next}
				<a href="{$sPages.next|rewrite:$sCategoryInfo.name}" title="{s name='ListingLinkNext' namespace='frontend/listing/listing_actions'}{/s}" class="navi more"><i class="fa fa-angle-right"></i></a>
			{/if}
		</div>

		{block name='frontend_listing_actions_count'}
		{* Count sites *}
		<div class="display_sites">
			{se name="ListingTextSite" namespace='frontend/listing/listing_actions'}Seite{/se} <strong>{$sPage}</strong> {se name="ListingTextFrom" namespace='frontend/listing/listing_actions'}von{/se} <strong>{$sNumberPages}</strong>
		</div>
		{/block}
	</div>
	{/if}
{/block}
{block name="frontend_listing_actions_close"}
</div>
<div class="space">&nbsp;</div>
{/block}
{else}
	{if $sCategoryContent.parent != 1}
	<div class="listing_actions normal">
		<div class="top">
			<a class="offers" href="{url controller='cat' sPage=1 sCategory=$sCategoryContent.id}">
				{s name="ListingActionsOffersLink" namespace='frontend/listing/listing_actions'}Weitere Artikel in dieser Kategorie &raquo;{/s}
			</a>
		</div>
	</div>
	<div class="space">&nbsp;</div>
	{/if}
{/if}
