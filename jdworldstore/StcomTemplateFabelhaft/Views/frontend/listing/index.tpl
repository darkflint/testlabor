{extends file='parent:frontend/listing/index.tpl'}

{block name="frontend_listing_index_banner" prepend}
	{* Breadcrumb *}
	{block name='frontend_index_breadcrumb'}
		{include file='frontend/index/breadcrumb.tpl'}
	{/block}
{/block}

{* Tagcloud REMOVED *}
{block name="frontend_listing_index_tagcloud"}
{/block}

{block name='frontend_index_content' append}
{/block}

{* Trusted shops logo *}
{block name='frontend_index_left_trustedshops'}
    {block name="frontend_listing_left_additional_features"}
        {include file="frontend/listing/right.tpl"}
        <div class="clear">&nbsp;</div>
    {/block}
{/block}

{block name="frontend_listing_index_text"}
{/block}
