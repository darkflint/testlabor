{if !$sOffers}

{block name="frontend_listing_actions_class"}
<div class="listing_actions{if !$sPages || $sPages.numbers|@count <= 1 || $sNumberPages <= 1} normal{/if}">
{/block}
{block name='frontend_listing_actions_top'}
	<div class="top">

		{if $sPropertiesOptionsOnly|@count || $sSuppliers|@count>1}<div class="t2fb_toggleButton"></div>{/if}
		{* Article per page *}
		{block name='frontend_listing_actions_items_per_page'}
		{if $sPerPage}
			<form method="get" action="{url controller=cat sCategory=$sCategoryContent.id}">
            {foreach from=$categoryParams key=key item=value}
                {if $key == 'sPerPage'}
                    {continue}
                {/if}
                <input type="hidden" name="{$key}" value="{$value}">
            {/foreach}
            <input type="hidden" name="sPage" value="1">
            <div class="articleperpage{if $sCategoryContent.noViewSelect} rightalign{/if}">
				<label>{s name='ListingLabelItemsPerPage' namespace='frontend/listing/listing_actions'}{/s}</label>
				<select name="sPerPage" class="auto_submit">
				{foreach from=$sPerPage item=perPage}
			        <option value="{$perPage.value}" {if $perPage.markup}selected="selected"{/if}>{$perPage.value}</option>
				{/foreach}
				</select>
				<div class="listing-action-icoplus"></div> 
			</div>
			</form>
		{/if}
		{/block}

		{* Sort filter *}
		{block name='frontend_listing_actions_sort'}
			<form method="get" action="{url controller=cat sCategory=$sCategoryContent.id}">
            {foreach from=$categoryParams key=key item=value}
                {if $key == 'sSort'}
                    {continue}
                {/if}
                <input type="hidden" name="{$key}" value="{$value}">
            {/foreach}
            <input type="hidden" name="sPage" value="1">
			<div class="sort-filter">
				<label>{s name='ListingLabelSort' namespace='frontend/listing/listing_actions'}{/s}</label>
				<select name="sSort" class="auto_submit">
					<option value="1"{if $sSort eq 1} selected="selected"{/if}>{s name='ListingSortRelease' namespace='frontend/listing/listing_actions'}{/s}</option>
					<option value="2"{if $sSort eq 2} selected="selected"{/if}>{s name='ListingSortRating' namespace='frontend/listing/listing_actions'}{/s}</option>
					<option value="3"{if $sSort eq 3} selected="selected"{/if}>{s name='ListingSortPriceLowest' namespace='frontend/listing/listing_actions'}{/s}</option>
					<option value="4"{if $sSort eq 4} selected="selected"{/if}>{s name='ListingSortPriceHighest' namespace='frontend/listing/listing_actions'}{/s}</option>
					<option value="5"{if $sSort eq 5} selected="selected"{/if}>{s name='ListingSortName' namespace='frontend/listing/listing_actions'}{/s}</option>
					{block name='frontend_listing_actions_sort_values'}{/block}
				</select>
				<div class="listing-action-icoplus"></div> 
			</div>
			</form>
		{/block}


		{* Change layout *}
		{block name="frontend_listing_actions_change_layout"}
		{if !$sCategoryContent.noViewSelect}
			<div class="list-settings">
                <label>{s name='ListingActionsSettingsTitle' namespace='frontend/listing/listing_actions'}Darstellung:{/s}</label>
                <a href="{url params=$categoryParams sViewport='cat' sCategory=$sCategoryContent.id sPage=1 sTemplate='list'}" class="list-view {if $sBoxMode=='list'}active{/if}" title="{s name='ListingActionsSettingsList' namespace='frontend/listing/listing_actions'}Listen-Ansicht{/s}"><i class="fa fa-bars fa-2x"></i></a>
            	<a href="{url params=$categoryParams sViewport='cat' sCategory=$sCategoryContent.id sPage=1 sTemplate='table'}" class="table-view {if $sBoxMode=='table'}active{/if}" title="{s name='ListingActionsSettingsTable' namespace='frontend/listing/listing_actions'}Tabellen-Ansicht{/s}"><i class="fa fa-th-large fa-2x"></i></a>
            </div>
		{/if}
		{/block}
		
		<noscript>
			<input type="submit" class="buttonkit green small rounded" value="OK" />
		</noscript>
	</div>
{/block}

{block name="frontend_listing_actions_close"}
</div>
<div class="space">&nbsp;</div>
{/block}
{else}
	{if $sCategoryContent.parent != 1}
	<div class="listing_actions normal">
		<div class="top">
			<a class="offers" href="{url controller='cat' sPage=1 sCategory=$sCategoryContent.id}">
				{s name="ListingActionsOffersLink" namespace='frontend/listing/listing_actions'}Weitere Artikel in dieser Kategorie &raquo;{/s}
			</a>
		</div>
	</div>
	<div class="space">&nbsp;</div>
	{/if}
{/if}
