{extends file='parent:frontend/listing/box_article.tpl'}

{* Article rating *}
        {block name='frontend_listing_box_article_rating'}
	    {/block}

{* New *}
{block name='frontend_listing_box_article_new'}
	{if $sArticle.newArticle}
	<div class="ico_new" {if $sArticle.pseudoprice}{/if}>{se name='ListingBoxNew'}{/se}</div>
	{/if}
{/block}

{* Article picture *}
{block name='frontend_listing_box_article_picture'}
		{if $sTemplate eq 'listing-3col' || $sTemplate eq 'listing-2col'}
			{assign var=image value=$sArticle.image.src.3}
		{else}
			{assign var=image value=$sArticle.image.src.2}
		{/if}
	<div class="box-article-image-wrap">
		<a href="{$sArticle.linkDetails|rewrite:$sArticle.articleName}" title="{$sArticle.articleName}" class="artbox_thumb" {if isset($sArticle.image.src)} 
			style="background: url({$image}) no-repeat center center"{/if}>
		{if !isset($sArticle.image.src)}<img src="{link file='frontend/_resources/images/no_picture.jpg'}" alt="{s name='ListingBoxNoPicture'}{/s}" />{/if}</a>
	</div>

{/block}

{* Article name *}
{block name='frontend_listing_box_article_name'}
	<a href="{$sArticle.linkDetails|rewrite:$sArticle.articleName}" class="title" title="{$sArticle.articleName}">{$sArticle.articleName|truncate:43}</a>
{/block}

{* Description *}
{block name='frontend_listing_box_article_description'}
	{if $sTemplate eq 'listing-1col'}
		{assign var=size value=0}
	{else}
		{assign var=size value=0}
	{/if}
	{if $sTemplate eq 'listing-1col'}
	<p class="desc">
		{if $sTemplate}
			{$sArticle.description_long|strip_tags|truncate:$size}
		{/if}
	</p>
	{/if}
{/block}

{* Unit price REMOVED *}
{block name='frontend_listing_box_article_unit'}
{/block} 

{* Article Price *}
{block name='frontend_listing_box_article_price'}
<p class="{if $sArticle.pseudoprice}pseudoprice{else}price{/if}{if !$sArticle.pseudoprice} both{/if}">
    {if $sArticle.pseudoprice}
    	<span class="pseudo">{s name="reducedPrice"}Statt {/s}{$sArticle.pseudoprice|currency}{s name="Star"}*{/s}</span>
    {/if}
    <span class="price">{if $sArticle.priceStartingFrom && !$sArticle.liveshoppingData}{s name='ListingBoxArticleStartsAt'}{/s} {/if}{$sArticle.price|currency}{s name="Star"}*{/s}</span>
</p>
{/block}


{* Compare and more *}
{block name='frontend_listing_box_article_actions'}
       	<div class="actions">
       		<div class="actions-left">
       		{block name='frontend_listing_box_article_actions_buy_now'}
			{* Buy now button *}
				<a href="{url controller='note' action='add' ordernumber=$sArticle.ordernumber}" class="note" rel="nofollow" title="{s name='ListingLinkNotepad'}Merkzettel{/s}">
					{se name="ListingLinkNotepad"}Merkzettel{/se}
				</a>
			{/block}
       		</div>
       		
       		{block name='frontend_listing_box_article_actions_inline'}
       			{* More informations button *}
				<a href="{$sArticle.linkDetails|rewrite:$sArticle.articleName}" title="{$sArticle.articleName}" class="more"><span>{s name='ListingBoxLinkDetails'}{/s}</span></a>
       		{/block}
       		
		</div>
{/block}




