(function($) {
	$.fn.CloudZoom.defaults.zoomWidth = 410;
	$.fn.CloudZoom.defaults.zoomHeight = 400;
})(jQuery);
 
$(document).ready(function() {
	t2fab_navInit();

});


function t2fab_navInit() {
	$("#t2mainNavigation li.tl").mouseenter(function() {
		t2fab_navMove(this);
	}).mouseleave(function() {
		t2fab_navMove("#t2mainNavigation li.active");
	});
	$(window).resize(function() {
		t2fab_navMove("#t2mainNavigation li.active");
	});
	t2fab_navMove("#t2mainNavigation li.active");
}

function t2fab_navMove(el) {
	if ($(el).length > 0) {
		var me = $(el),
			pos = me.position(),
			oleft = pos.left,
			width = me.width(),
			shapew = $(".navishape").width(),
			shape = (width/2 + oleft) - shapew/2;
		
		$(".navishape").css('left',  shape + 'px');
	}
	else {
		var shapew = $(".navishape").width();
		$(".navishape").css('left',  '-' + shapew + 'px');
	}
}
