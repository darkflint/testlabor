{extends file='frontend/listing/listing_actions_bottom.tpl'}

{block name="frontend_listing_actions_class"}
<div class="listing_actions{if !$sPages || $sPages.count <= 1} normal{/if}">
{/block}
{block name="frontend_listing_actions_change_layout"}
{/block}
{* Listing paging *}
{block name='frontend_listing_actions_paging'}
{if $sPages.pages|@count != 0}
	<div class="bottom">
		<div class="paging">
			<label>{s name='ListingPaging' namespace='frontend/search/paging' }{/s}</label>
			{if isset($sPages.before)}
				<a href="{$sLinks.sPage}&sPage={$sPages.before}" title="{s name='ListingLinkNext' namespace='frontend/search/paging'}{/s}" class="navi prev">
					<i class="fa fa-angle-left"></i>
				</a>
			{/if}
			{foreach from=$sPages.pages item=page}
                {if $page<$sRequests.currentPage+4 AND $page>$sRequests.currentPage-4}
                    {if $sRequests.currentPage==$page}
                        <a title="{$sCategoryInfo.name}" class="navi on">{$page}</a>
                        {else}
                        <a href="{$sLinks.sPage}&sPage={$page}" title="{$sCategoryInfo.name}" class="navi">
                            {$page}
                        </a>
                    {/if}
                    {elseif $page==$sRequests.currentPage+4 OR $page==$sRequests.currentPage-4}
                    <div class="more">...</div>
                {/if}
			{/foreach}
			{if $sPages.next}
				<a href="{$sLinks.sPage}&sPage={$sPages.next}" title="{s name='ListingLinkPrevious' namespace='frontend/search/paging'}{/s}" class="navi more">
					<i class="fa fa-angle-right"></i>
				</a>
			{/if}
		</div>
	</div>
{/if}
{/block}

{block name='frontend_listing_actions_sort'}
{/block}

{block name='frontend_listing_actions_items_per_page'}
{/block}