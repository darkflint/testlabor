{extends file='parent:frontend/search/fuzzy.tpl'}

{block name='frontend_search_fuzzy_result'}
		<div class="result_box">
			{s name='SearchHeadline'}Zu "{$sRequests.sSearch}" wurden {$sSearchResults.sArticlesCount} Artikel gefunden!{/s}
		</div>
		
		{include file='frontend/search/filter_category.tpl'}
		
		<div class="grid_13 first last">
			{* Listing Actions *}
			{include file='frontend/search/paging_top.tpl' sTemplate='listing'}
			
			{* Actual listing *}
			<div class="listing" id="listing-3col">
				{foreach from=$sSearchResults.sArticles item=sArticle key=key name=list}
					{include file='frontend/listing/box_article.tpl' sTemplate='listing'}
				{/foreach}
				<div class="clear">&nbsp;</div>
			</div>
			
			{* Pagination *}
			{include file='./frontend/search/paging_bottom.tpl'}
		</div>
{/block}
		
