{extends file='frontend/listing/listing_actions_top.tpl'}

{block name="frontend_listing_actions_class"}
<div class="listing_actions{if !$sPages || $sPages.count <= 1} normal{/if}">
{if $sSearchResults.sSuppliers} <div class="t2fb_toggleButton"></div> {/if}
{/block}
{block name="frontend_listing_actions_change_layout"}
{/block}
{* Listing paging *}
{block name='frontend_listing_actions_paging'}
{if $sPages.pages|@count != 0}
{/if}
{/block}

{block name='frontend_listing_actions_sort'}
<form name="frmsort" method="post" action="{$sLinks.sSort}">
	<div class="sort-filter">
		<label for="sSort">{s name='ListingLabelSort' namespace='frontend/search/paging'}{/s}</label>
		<select name="sSort" id="sSort" class="auto_submit">
			<option value="6"{if $sRequests.sSort eq 6} selected="selected"{/if}>{s name='ListingSortRelevance' namespace='frontend/search/paging'}{/s}</option>
			<option value="1"{if $sRequests.sSort eq 1} selected="selected"{/if}>{s name='ListingSortRelease' namespace='frontend/search/paging'}{/s}</option>
			<option value="7"{if $sRequests.sSort eq 7} selected="selected"{/if}>{s name='ListingSortRating' namespace='frontend/search/paging'}{/s}</option>
			<option value="3"{if $sRequests.sSort eq 3} selected="selected"{/if}>{s name='ListingSortPriceLowest' namespace='frontend/search/paging'}{/s}</option>
			<option value="4"{if $sRequests.sSort eq 4} selected="selected"{/if}>{s name='ListingSortPriceHighest' namespace='frontend/search/paging'}{/s}</option>
			<option value="5"{if $sRequests.sSort eq 5} selected="selected"{/if}>{s name='ListingSortName' namespace='frontend/search/paging'}{/s}</option>
		</select>
	</div>
</form>	
{/block}

{block name='frontend_listing_actions_items_per_page'}
	{if $sPerPage}
		<form method="post" action="{$sLinks.sPerPage}">
		<div class="articleperpage right">
			<label>{s name='ListingLabelItemsPerPage' namespace='frontend/search/paging' }Artikel pro Seite:{/s}</label>
			<select name="sPerPage" class="auto_submit">
			{foreach from=$sPerPage item=perPage}
		        <option value="{$perPage}" {if $perPage eq $sRequests.sPerPage}selected="selected"{/if}>{$perPage}</option>
			{/foreach}
			</select>
		</div>
		</form>
	{/if}
{/block}