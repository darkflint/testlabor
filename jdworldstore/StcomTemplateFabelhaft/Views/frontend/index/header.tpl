{extends file='parent:frontend/index/header.tpl'}

{block name="frontend_index_header_css_screen" append}
	<link type="text/css" media="screen, projection" rel="stylesheet" href="{link file='frontend/_resources/fa/css/font-awesome.min.css'}"/>
	<link type="text/css" media="screen, projection" rel="stylesheet" href="{link file='frontend/_resources/linecons/linecons.css'}" />
	<link type="text/css" media="screen, projection" rel="stylesheet" href="{link file='frontend/_resources/flaticons/flaticon.css'}" />

	<link href='//fonts.googleapis.com/css?family=Oswald:400,300' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Lato:300,400,700,400italic' rel='stylesheet' type='text/css'>
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	

		<link id="t2fabelhaftStyleSheet" type="text/css" media="screen, projection" rel="stylesheet" href="{link file='frontend/_resources/css/style.css'}"/>

	<!--[if gte IE 9]>
	  <style type="text/css">
	    div#notes .table_row .action, .artbox .actions, #content #left .filter_search h3.heading, .ajax_login_form, #buybox .basketButton-wrap:hover, #buybox .basketButton-wrap, #detail #detailbox .imgwrap, #detail .article_back:hover a, #detail .article_back a, .articlenavigation, .artbox .actions, .artbox .box-article-image-wrap, #infobar .header, #content #left .headingbox_nobg.filter_properties, .mainNavigation_wrap, .gradient {
	       filter: none;
	    }
	  </style>
	<![endif]-->
	
	
	<!-- DL Menu-->
	<link type="text/css" media="screen, projection" rel="stylesheet" href="{link file='frontend/_resources/dlmenu/css/component.css'}"/>
	<!-- /DL Menu-->

	<style>
		{if $fabelhaft.logo}
			#header #logo a {
				background-image: url({link file={$fabelhaft.logo}});
			}
			{if $fabelhaft.logoretina}
				@media (-webkit-min-device-pixel-ratio: 2) {
					#header #logo a { background-image:url({link file={$fabelhaft.logoretina}});}
				}
			{else}
				@media (-webkit-min-device-pixel-ratio: 2) {
					#header #logo a { background-image:url({link file={$fabelhaft.logo}});}
				}
			{/if}
		{/if}
	</style>
	{* Print Stylesheets *}
	{block name="frontend_index_header_css_print"}
		<link type="text/css" rel="stylesheet" media="print" href="{link file='frontend/_resources/styles/print.css'}" />
	{/block}

{/block}

{block name="frontend_index_header_javascript_jquery" append}

	<script type="text/javascript" src="{link file='frontend/_resources/javascript/jquery.fn.js'}"></script>
	<script type="text/javascript" src="{link file='frontend/_resources/javascript/t2_fabelhaft.js'}"></script>
	
	
	<!-- DL Menu-->
	<script type="text/javascript" src="{link file='frontend/_resources/dlmenu/js/modernizr.custom.js'}"></script>
	<script type="text/javascript" src="{link file='frontend/_resources/dlmenu/js/jquery.dlmenu.js'}"></script>
	<!-- /DL Menu -->
	
	<script>
		t2templateController = '{url controller="widgets" action="StcomFabelhaft"}';
	</script>
{/block}


{* Meta-Tags *}
{block name='frontend_index_header_meta_tags' append}
	<link rel="shortcut icon" href="{s name='IndexMetaShortcutIcon'}{link file='frontend/_resources/favicon.ico'}{/s}" type="image/x-icon" />{* Favicon *}
{/block}
