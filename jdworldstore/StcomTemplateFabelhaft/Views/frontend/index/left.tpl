{extends file='parent:frontend/index/left.tpl'}

	{* Trusted shops logo *}
	{block name='frontend_index_left_trustedshops'}
	{/block}
	
{block name='frontend_index_left_campaigns_bottom'}

{s name='LeftSideBanner'}
	<img class="sidebanner" src="{link file='frontend/_resources/images/banner_trustedshops.jpg'}">
{/s}
    {include file="frontend/campaign/box.tpl" campaignsData=$campaigns.leftBottom}

{/block}
