<div id="footer">

	{block name='frontend_index_footer_menu'}
		{include file='frontend/index/menu_footer.tpl'}

		<div class="clear"></div>
	{/block}

</div>

	{block name='frontend_index_footer_copyright'}
	<div class="bottom">
		{block name='frontend_index_footer_vatinfo'}
		{/block}
		<div class="footer_copyright">
			<a href="http://shop-templates.com" title="Responsive Templates" class="logoT2" target="_blank" rel="">Responsive Shop Template</a>
		</div>
	</div>
	{/block}
