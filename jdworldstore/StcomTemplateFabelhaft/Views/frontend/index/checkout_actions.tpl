

<div id="shopnavi">
	<div class="shopnavi-account">
        {block name="frontend_index_checkout_actions_account"}
        <a href="{url controller='account'}" title="{s namespace='frontend/index/checkout_actions' name='IndexLinkAccount'}{/s}" class="account">
            {s namespace='frontend/index/checkout_actions' name='IndexLinkAccount'}{/s}
        </a>
        {/block}

        {block name="frontend_index_checkout_actions_notepad"}
		<a href="{url controller='note'}" title="{s namespace='frontend/index/checkout_actions' name='IndexLinkNotepad'}{/s}" {if $sNotesQuantity > 0}{/if} class="note">
			{s namespace='frontend/index/checkout_actions' name='IndexLinkNotepad'}{/s} {if $sNotesQuantity > 0}<span class="notes_quantity">{$sNotesQuantity}</span>{/if}
		</a>
        {/block}		
	</div>
    
	{block name="frontend_index_checkout_actions_cart"}
    <div class="grid_6 newbasket{if $sBasketQuantity} active{/if}">
    
		<div class="grid_2 last icon">
			<a href="{url controller='checkout' action='cart'}" title="{s namespace='frontend/index/checkout_actions' name='IndexLinkCart'}{/s}">
				{if $sUserLoggedIn}{s name='IndexLinkCheckout'}{/s}{else}{s namespace='frontend/index/checkout_actions' name='IndexLinkCart'}{/s}{/if}
			</a>
		</div>

		<div class="grid_5 first display">
			<div class="basket_left">
				<span>
					<a href="{url controller='checkout' action='cart'}" title="{s namespace='frontend/index/checkout_actions' name='IndexLinkCart'}{/s}">
						{s namespace='frontend/index/checkout_actions' name='IndexLinkCart'}{/s}
					</a>
				</span>
			</div>
		</div>
		
		<div class="ajax_basket_container hide_script">
			<div class="ajax_basket">
				{s name='IndexInfoArticles'}{/s}
				<span class="amount">{$sBasketAmount|currency}*</span>
				
				
				{* Ajax loader *}
				<div class="ajax_loader">&nbsp;</div>
			</div>
		</div>

		<a href="{url controller='checkout' action='cart'}" class="quantity">{$sBasketQuantity}</a>
		
        <div class="clear">&nbsp;</div>
    </div>
	{/block}
	
    {block name="frontend_index_checkout_actions_inner"}{/block}
    
</div>
