  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="apple-mobile-web-app-title" content="Fabelhaft">

  <link rel="apple-touch-icon" href="{link file='frontend/_resources/app/app60.png'}">
  <link rel="apple-touch-icon" sizes="76x76" href="{link file='frontend/_resources/app/app76.png'}">
  <link rel="apple-touch-icon" sizes="120x120" href="{link file='frontend/_resources/app/app120.png'}">
  <link rel="apple-touch-icon" sizes="152x152" href="{link file='frontend/_resources/app/app152.png'}">