{extends file='parent:frontend/checkout/ajax_cart.tpl'}


			{block name='frontend_checkout_ajax_cart_row'}
			
			<div class="{if $sBasketItem.modus == 1} premium{elseif $sBasketItem.modus == 10} bundle{/if}{if $smarty.foreach.ajaxbasket.last} last{/if}">
				{if $sBasketItem.image.src.0}
				<div class="thumbnail">
					<img src="{$sBasketItem.image.src.0}" alt="{$sBasketItem.articlename|strip_tags}" />
				</div>
				{/if}
				
				{* Article name *}
				{block name='frontend_checkout_ajax_cart_articlename'}
				<span class="title">
					<strong>{$sBasketItem.quantity}x</strong> <a href="{$sBasketItem.linkDetails}" title="{$sBasketItem.articlename|strip_tags}">
					{if $sBasketItem.modus == 10}{se name='AjaxCartInfoBundle'}{/se}{else}{$sBasketItem.articlename|truncate:25}{/if}
					</a>
				</span>
				{/block}
				
				{block name='frontend_checkout_ajax_cart_price'}
				{* Article price *}
				<strong class="price">{if $sBasketItem.amount}{$sBasketItem.amount|currency}{else}{se name="AjaxCartInfoFree"}{/se}{/if}*</strong>
				{/block}
				
				
			</div>
			{/block}
