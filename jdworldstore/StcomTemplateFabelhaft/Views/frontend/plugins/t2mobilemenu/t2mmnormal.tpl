{function name=t2mmLayer}
	<ul class="dl-submenu">
		{if $parent}<li><a href="{$parent.link}">{$parent.name}</a></li>{/if}
		{foreach from=$categories item=category}
		    <li>
		     	<a href="{$category.link}">{$category.name}</a>
		    	{if $category.sub}
			     	{call name=t2mmLayer categories=$category.sub parent=$category}
			    {/if}
		    </li>
		{/foreach}
	</ul>
{/function}

<ul class="dl-menu" style="display:none;">
	<li>
        <a href="{url controller='index'}" title="{s name='IndexLinkHome' namespace='frontend/index/categories_top'}{/s}" class="first{if $sCategoryCurrent eq $sCategoryStart} active{/if}">
            {s name='IndexLinkHome' namespace='frontend/index/categories_top'}Home{/s}
        </a>
    </li>
    {foreach from=$sMainCategories item=sCategory}
		<li>
        	<a href="{$sCategory.link}" title="{$sCategory.description}">
        		{$sCategory.name}
        	</a>
        	{if $sCategory.sub}
		     	{call name=t2mmLayer categories=$sCategory.sub parent=$sCategory}
		    {/if}
        </li>
	{/foreach}
	{if $sMenu}
		<li>
        	<a href="#">
        		Service
        	</a>
        	<ul class="dl-submenu">
			{foreach from=$sMenu.gLeft item=item}
				<li>
			     	<a href="{if $item.link}{$item.link}{else}{url controller=custom sCustom=$item.id title=$item.description}{/if}" title="{$item.description}">{$item.description}</a>
			    </li>
			{/foreach}
			</ul>
	{/if}
</ul>

