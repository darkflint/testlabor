<script>
	$(document).ready(function() {
			$('.flexsliderManufacturer{$Data.objectId}').flexslider({
				itemWidth: 240,
				itemMargin: 0,
				animation: "slide",
    			animationLoop: false,
			});
	});
</script>
<div class="flexslider flexsliderManufacturer flexsliderManufacturer{$Data.objectId} " style="width: 100%;">
  {if $Data.manufacturer_slider_title}<span class="title">{$Data.manufacturer_slider_title}</span>{/if}
  <ul class="slides">
    {foreach $Data.values as $supplier name="foreachManufacturer"}
        <li style="left:{(($smarty.foreach.foreachManufacturer.iteration)*240)-240}px">
            	<a href="{$supplier.link}" title="{$supplier.name}" class="supplierBox {if $Data.manufacturer_slider_title}with_title {/if}" {if $supplier.image}style="background-image:url('{link file={$supplier.image}}');"{/if}>
                    {if !$supplier.image}
                        <span>{$supplier.name}</span>
                    {/if}
            	</a>
        </li>
    {/foreach}
  </ul>
</div>

