{extends file='parent:frontend/listing/box_article.tpl'}


{* Article name *}
		{block name='frontend_listing_box_article_name'}
		<a href="{$sArticle.linkDetails|rewrite:$sArticle.articleName}" class="title" title="{$sArticle.articleName}">{$sArticle.articleName|truncate:43}</a>
		{/block}

{* Article picture *}
{block name='frontend_listing_box_article_picture'}
		
	{* 3 spalter bilder *}
	{if $sTemplate eq 'listing-3col'}
		{* 1/3 *}
		{if $colWidth eq 3}
			{assign var=image value=$sArticle.image.src.4}
		{* 2/3 *}
		{elseif $colWidth eq 2}
			{assign var=image value=$sArticle.image.src.3}
		{* 3/3 *}
		{else}
			{assign var=image value=$sArticle.image.src.3}
		{/if}
		
	{* 4 spalter *}
	{else}
		{* 1/4 *}
		{if $colWidth eq 4}
			{assign var=image value=$sArticle.image.src.4}
		{* 2/4 *}
		{elseif $colWidth eq 3}
			{assign var=image value=$sArticle.image.src.4}
		{* 3/4 *}
		{elseif $colWidth eq 2}
			{assign var=image value=$sArticle.image.src.3}
		{* 4/4 *}
		{else}
			{assign var=image value=$sArticle.image.src.3}
		{/if}
	{/if}
	<div class="box-article-image-wrap">
		<a href="{$sArticle.linkDetails|rewrite:$sArticle.articleName}" title="{$sArticle.articleName}" class="artbox_thumb" {if isset($sArticle.image.src)} 
			style="background: url({$image}) no-repeat center center"{/if}>
		{if !isset($sArticle.image.src)}<img src="{link file='frontend/_resources/images/no_picture.jpg'}" alt="{s name='ListingBoxNoPicture'}{/s}" />{/if}</a>
	</div>
	<div class="box-article-image-hover">
		{if $sArticle.attr1}
			<div class="format">{$sArticle.attr1}</div>
		{/if}
	</div>
{/block}

{* Increase the size of the description text *}
{block name='frontend_listing_box_article_description'}
					
		{* 3 spalter *}
		{if $sTemplate eq 'listing-3col'}
			{if $colWidth eq 3}
				{assign var=size value=850}
			{elseif $colWidth eq 2}
				{assign var=size value=200}
			{else}
				{assign var=size value=0}
			{/if}
	
		{* 4 spalter *}
		{else}
			{if $colWidth eq 4}
				{assign var=size value=850}
			{elseif $colWidth eq 3}
				{assign var=size value=300}
			{elseif $colWidth eq 2}
				{assign var=size value=0}
			{else}
				{assign var=size value=0}
			{/if}
		{/if}
	
	<p class="desc">
	    {$Data.description_long|strip_tags|truncate:$size}
	</p>
	
{/block}

{* Unit price *}
{block name='frontend_listing_box_article_unit'}
{/block}    	
