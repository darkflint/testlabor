<script>
	$(document).ready(function() {
		$(".flexsliderBanner-{$Data.objectId}").flexslider({
			animationSpeed : '{$Data.banner_slider_scrollspeed}',
			animation : 'slide',
			itemMargin : 0,
		});
	})
</script><div class="flexslider flexsliderBanner-{$Data.objectId}">
	<ul class="slides">
		{foreach $Data.values as $banner}
			<li>
				{if $banner.link}
					<a href="{$banner.link}">
	                    <img src="{$banner.path}" alt="{$banner.altText}" {if $banner.title}title="{$banner.title}" {/if}/>
	                </a>
				{else}
					<img src="{$banner.path}" alt="{$banner.altText}" {if $banner.title}title="{$banner.title}" {/if}/>
				{/if}
			</li>
		{/foreach}
	</ul>
</div>