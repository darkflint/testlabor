{$perPage = $sColWidth }
{$sSliderElementWidth = $sElementWidth/$perPage }
<script>
	$(document).ready(function() {
			$('.flexsliderArticle_{$Data.objectId}').flexslider({
				itemWidth: 320,
				itemMargin: 0,
				animation: "slide",
    			animationLoop: false,
			});
	});
</script>
<div class="flexslider flexsliderArticle flexsliderArticle_{$Data.objectId} {if $Data.article_slider_title} with_title {/if}" style="width: 100%;">
  {if $Data.article_slider_title}<span class="title">{$Data.article_slider_title}</span>{/if}
  <ul class="slides">
  	{assign "sliderArticles" $Data.values}
	{if $Data.article_slider_type == 'topseller'}
		{assign "sliderArticles" $Data.topsellerArticles}
	{elseif $Data.article_slider_type == 'newcomer'}
		{assign "sliderArticles" $Data.newcomerArticles}
	{/if}
    {foreach $sliderArticles|array_chunk:$perPage as $articles}
        {include file="widgets/emotion/slide_articles.tpl" articles=$articles sElementWidth=$sElementWidth sPerPage=$perPage sElementHeight=$sliderHeight-5}
    {/foreach}
  </ul>
</div>

