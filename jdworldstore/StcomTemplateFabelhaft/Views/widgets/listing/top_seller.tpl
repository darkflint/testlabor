{if $sCharts|@count}
{$perPage = $sColWidth }
{$sSliderElementWidth = $sElementWidth/$perPage }
<script>
	$(document).ready(function() {
			$('.flexSliderTopSeller').flexslider({
				itemWidth: 250,
				itemMargin: 0,
				animation: "slide",
    			animationLoop: false,
			});
	});
</script>
<div class="listing-slider-wrapper">
<h2 class="headingbox_nobg">{s name="TopsellerHeading" namespace=frontend/plugins/index/topseller}{/s}</h2>
<div class="flexslider flexSliderTopSeller" style="width: 100%; {* {if $Data.article_slider_title}padding-top: 23px;{/if} *}">
  <ul class="slides">
	{foreach from=$sCharts item=article}
			<li>
				{assign var=image value=$article.image.src.4 }
		        <div class="articleSliderBox">
		        	<div class="innerSlideBox">
			            <!-- article 1 -->
			            {if $image}
			            <a class="thumbnail" title="{$article.articleName}" href="{$article.linkDetails}" style="background-image: url({$image});"></a>
			            {else}
			            <a class="article-thumb-wrapper" title="{$article.articleName}" href="{$article.linkDetails}" style="background-image:url({link file='frontend/_resources/images/no_picture.jpg'});"></a>
			            {/if}
			            <a title="{$article.articleName}" class="title" href="{$article.linkDetails}">{$article.articleName|truncate:35}</a>
			
						{if $article.pseudoprice} <span class="pseudoPrice">{s name="reducedPrice"}Statt: {/s}{$article.pseudoprice|currency} {s name="Star"}*{/s}</span>{/if}
			            <span class="articlePrice">{if $article.priceStartingFrom && !$article.liveshoppingData}{s namespace="frontend/plugins/recommendation/slide_articles" name='ListingBoxArticleStartsAt'}{/s} {/if}{$article.price|currency} *</span>
		        	</div>
		        </div>
			</li>
	{/foreach}
  </ul>
</div>
</div>

{/if}

