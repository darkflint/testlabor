{extends file='parent:widgets/index/shop_menu.tpl'}

{block name='frontend_index_actions_active_shop'}
	<div class="topbar_lang">
		<form method="post" action="">
			<input type="hidden" name="__shop" id="shop_set_language_field" value="">
			{if $languages|@count > 1}
				{foreach from=$languages item=language}
		        		 <div class="{$language->getLocale()->toString()} flag {if $language->getId() === $shop->getId()}active{/if}" onClick="$('#shop_set_language_field').val('{$language->getId()}'); $(this).parent().submit();">{$shop->getName()}</div>
				{/foreach}
			{/if}
	    </form>
   </div>
{/block}
