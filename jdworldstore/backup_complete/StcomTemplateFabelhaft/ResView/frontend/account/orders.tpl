{extends file='parent:frontend/account/orders.tpl'}

{* Breadcrumb *}
{block name='frontend_index_start' append}
	{$sBreadcrumb[] = ['name'=>"{s name='MyOrdersTitle'}{/s}", 'link'=>{url}]}
{/block}

{* Main content *}
{block name='frontend_index_content'}

{* Orders overview *}
<div class="grid_16 orders" id="center">
	{if !$sOpenOrders}
	{block name="frontend_account_orders_info_empty"}
	    <h1>{se name='OrdersHeadline'}Meine Bestellungen{/se}</h1>
	
	    <fieldset>
	        <div class="notice center bold">
	            {se name="OrdersInfoEmpty"}{/se}
	        </div>
	    </fieldset>
	{/block}
	{else}
	<h1>{se name="OrdersHeader"}{/se}</h1>
	<div class="orderoverview_active">
	
		<div class="orderTable">
			{block name="frontend_account_orders_table_head"}
			<div class="orderTableHead">
				
				<div class="orderTableHeadCol col1">
					{se name="OrderColumnDate"}{/se}
				</div>
				
				<div class="orderTableHeadCol col2">
					{se name="OrderColumnId"}{/se}
				</div>
				<div class="orderTableHeadCol col3">
					{se name="OrderColumnDispatch"}{/se}
				</div>
				
				<div class="orderTableHeadCol col4">
					{se name="OrderColumnStatus"}{/se}
				</div>
				
				<div class="orderTableHeadCol col5">
					{se name="OrderColumnActions"}{/se}
				</div>
			</div>
			{/block}
			{foreach name=orderitems from=$sOpenOrders item=offerPosition}
				{if $smarty.foreach.orderitems.last}
					{assign var=lastitem value=1}
				{else}
					{assign var=lastitem value=0}
				{/if}
				{include file="frontend/account/order_item.tpl" lastitem=$lastitem}
			{/foreach}
		</div>
	</div>
	<div class="space">&nbsp;</div>
	{/if}
</div>
{/block}