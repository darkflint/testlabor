(function($) {
$(document).ready(function() {
	// Twenty2ResponsiveEmotion
	t2re_init();
});

/* twenty2 responsive emotion t2re */
function t2re_init() {
	// Step 1 scan all grids
	t2re_grids = [];
	$(".t2re-grid").each(function() {
		
		// Child Elements
		var gridelements = [];
		$(this).children().each(function(){
			var el = {
				'el' : this,
				'width' : $(this).width(),
				'height' : $(this).height(),
				'top' : $(this).position().top,
				'left' : $(this).position().left,
				'heightfix' : $(this).data('heightfix'),
				'ratio' : $(this).height()/($(this).width())
			};
			gridelements.push(el);
		});
		
		// Grid attributes
		var gridattributes = {
			'els' : gridelements,
			'rowwidth' : $(this).data('rowwidth')-10, // -10 shopware overflow fix
			'rowheight' : $(this).data('rowheight'),
			'gridratio' : $(this).height()/($(this).data('rowwidth')),
			'id' : "#" + $(this).attr('id')
		};
		
		
		$(this).addClass("scanned");
		t2re_grids.push(gridattributes);
	});
	
	// Resize Everything
	t2re_resize();
	$( window ).resize(function() {
		t2re_resize();
	});
}
function t2re_resize() {
	
	t2re_grids.forEach(function(grid) {
		var gridid = grid['id'];
		var gridrowwidth = grid['rowwidth'];
		var gridrowheight = grid['rowheight'];
		var gridratio = grid['gridratio'];
		var gridper = $(gridid).width()/gridrowwidth;
		var gridels = grid['els'];
		
		if (window.innerWidth < 641) {
			fullsize = true;
		}
		else {
			fullsize= false;
		}
		gridels.forEach(function(el) {
			t2re_elementResize(el,gridper,fullsize);
		});
		
		if (!fullsize) {
			$(gridid).css('height' , $(gridid).width()*gridratio + 'px');
		}
		else {
			$(gridid).css('height' , 'auto');
		}
	});
}


function t2re_elementResize(el,gridper,fullsize) {
	var eid = el['el'];
	var ewidth = el['width'];
	var eheight = el['height'];
	var etop = el['top'];
	var eleft = el['left'];
	var eratio = el['ratio'];
	var heightfix = el['heightfix'];
	if (fullsize) {
		if (heightfix) {
			$(eid).css('height' , heightfix + 'px');
		}
		else {
			$(eid).css('height' , $(eid).width()*eratio + 'px');
		}
	}
	else {
		$(eid).css('width' , ewidth*gridper + 'px');
		$(eid).css('height' , (ewidth*gridper)*eratio + 'px');
	}
	$(eid).css('left' , eleft*gridper + 'px');
	$(eid).css('top' , etop*gridper + 'px');
}

}(jQuery));