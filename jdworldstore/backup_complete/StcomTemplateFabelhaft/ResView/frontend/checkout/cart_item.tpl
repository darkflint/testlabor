{if $sBasketItem.modus != 1 && $sBasketItem.modus != 3 && $sBasketItem.modus != 10 && $sBasketItem.modus != 2 && $sBasketItem.modus != 4}
<div class="cart-item">
	<form name="basket_change_quantity{$sBasketItem.id}" method="post" action="{url action='changeQuantity' sTargetAction=$sTargetAction}">
		{* Article informations *}
		<div class="basketitemblock-a">
			<div class="item-block item-block-1">
				<div class="block-1-top">
					<div class="cartimg-wrap">
						{* Article picture *}
						{block name='frontend_checkout_cart_item_image'}
						{if $sBasketItem.image.src.0}
							<a href="{$sBasketItem.linkDetails}" title="{$sBasketItem.articlename|strip_tags}" class="thumb_image">
								<img src="{$sBasketItem.image.src.2}" border="0" alt="{$sBasketItem.articlename}" />
							</a>
						{else}
							<img class="no_image" src="{link file='frontend/_resources/images/no_picture.jpg'}" alt="{$sBasketItem.articlename}" />
						{/if}
						{/block}
					</div>
					<div class="cart-item-title-wrap">
						{block name='frontend_checkout_cart_item_details'}
						<div class="basket_details">
							{* Article name *}
							{if $sBasketItem.modus ==0}
								<a class="title" href="{$sBasketItem.linkDetails}" title="{$sBasketItem.articlename|strip_tags}">
									{$sBasketItem.articlename|strip_tags|truncate:60}
								</a>
								<p class="ordernumber">
									{se name="CartItemInfoId"}{/se} {$sBasketItem.ordernumber}
								</p>
								<p>
					                {s name='CheckoutItemPrice' namespace="frontend/checkout/confirm_item"}{/s} {$sBasketItem.price|currency} {s name="Star" namespace="frontend/listing/box_article"}{/s}
					            </p>
								{block name='frontend_checkout_cart_item_details_inline'}{/block}
							{else}
								{$sBasketItem.articlename}
							{/if}

							{* Delivery informations *}
							{block name='frontend_checkout_cart_item_delivery_informations'}
								<div class="delivery">
									{if {config name=BasketShippingInfo}}
										{if $sBasketItem.shippinginfo}
											{include file="frontend/plugins/index/delivery_informations.tpl" sArticle=$sBasketItem}
							    		{/if}
							    	{else}
							    		&nbsp;
						    		{/if}
					    		</div>
							{/block}
						</div>
						{/block}
					</div>
				</div>
				<div class="clear"></div>
				<div class="block-1-bottom">
					
					{block name='frontend_checkout_cart_item_main_features'}{/block}

				</div>
			</div>
		</div>
		<div class="basketitemblock-b">
			{block name='frontend_checkout_cart_item_quantity'}
			<div class="item-block item-block-2">
			{* Article amount *}
				{if $sBasketItem.modus == 0}
					<select name="sQuantity" class="auto_submit">
					{section name="i" start=$sBasketItem.minpurchase loop=$sBasketItem.maxpurchase+1 step=$sBasketItem.purchasesteps}
						<option value="{$smarty.section.i.index}" {if $smarty.section.i.index==$sBasketItem.quantity}selected="selected"{/if}>
								{$smarty.section.i.index} 
						</option>
					{/section}
					</select>
					<input type="hidden" name="sArticle" value="{$sBasketItem.id}" />
				{else}
					&nbsp;
				{/if}
				

			</div>
			{/block}
			
			
			{* Tax price *}
			{block name='frontend_checkout_cart_item_tax_price'}
			<div class="item-block item-block-3">
				{if $sUserData.additional.charge_vat}{$sBasketItem.tax|currency}{else}&nbsp;{/if}
			</div>
			{/block}
			
			{* Article total sum *}
			{block name='frontend_checkout_cart_item_total_sum'}
			<div class="item-block item-block-4">
				<strong>
					{$sBasketItem.amount|currency}*
				</strong>
			</div>
			{/block}
		
		</div>
		{block name='frontend_checkout_cart_item_delete_article'}
			<div class="action">
				{if !$baseUrl|strstr:"finish"}
					<a href="{url action='deleteArticle' sDelete=$sBasketItem.id sTargetAction=$sTargetAction}" class="del" title="{s name='CartItemLinkDelete '}{/s}">
						&nbsp;
					</a>
					&nbsp;
				{/if}
			</div>
		{/block}
		<div class="clear">&nbsp;</div>
	</form>
</div>

{* Voucher *}
{elseif $sBasketItem.modus == 2}
<div class="cart-item voucher">
	<div class="basketitemblock-a">
		{block name='frontend_checkout_cart_item_voucher_details'}
		<div class="item-block item-block-1">
			<div class="block-1-top">
				<div class="cartimg-wrap">
					<div class="voucher_img">&nbsp;</div>
				</div>
				<div class="cart-item-title-wrap">

					<div class="basket_details">

						<span class="title">{$sBasketItem.articlename}</span>
						<p class="ordernumber">
							{se name="CartItemInfoId"}{/se}: {$sBasketItem.ordernumber}
						</p>
					</div>
				</div>
			</div>
			<div class="clear"></div>
			
		</div>
		{/block}
	</div>
	<div class="basketitemblock-b">

		<div class="item-block item-block-2">
		</div>


		<div class="item-block item-block-3">
			{block name='frontend_checkout_cart_item_voucher_tax_price'}{/block}
		</div>

		{block name='frontend_checkout_cart_item_voucher_price'}
		<div class="item-block item-block-4">
			<strong>
				{if $sBasketItem.itemInfo}
					{$sBasketItem.itemInfo}
				{else}
					{$sBasketItem.price|currency} {block name='frontend_checkout_cart_tax_symbol'}*{/block}
				{/if}
			</strong>
		</div>
		{/block}

		{block name='frontend_checkout_cart_item_voucher_delete'}
			<div class="action">
				<a href="{url action='deleteArticle' sDelete=voucher sTargetAction=$sTargetAction}" class="del" title="{s name='CartItemLinkDelete'}{/s}">&nbsp;</a>
			</div>
		{/block}
		<div class="clear">&nbsp;</div>
	</div>
</div>


{* Basket rebate *}
{elseif $sBasketItem.modus == 3}
<div class="cart-item rebate">
		<div class="basketitemblock-a">
			<div class="item-block item-block-1">
				<div class="block-1-top">
					<div class="cartimg-wrap">
					</div>
					<div class="cart-item-title-wrap">
						{block name='frontend_checkout_cart_item_rebate_detail'}
						<div class="basket_details">
							<span class="title">{$sBasketItem.articlename}</span>
						</div>
						{/block}
					</div>
				</div>
				<div class="clear"></div>
				<div class="block-1-bottom">

				</div>
			</div>
		</div>
		<div class="basketitemblock-b">

			<div class="item-block item-block-2">
			</div>

			<div class="item-block item-block-3">
				{block name='frontend_checkout_cart_item_rebate_tax_price'}{/block}
			</div>
			{block name='frontend_checkout_cart_item_rebate_price'}
			<div class="item-block item-block-4">
				<strong>
					{if $sBasketItem.itemInfo}
						{$sBasketItem.itemInfo}
					{else}
						{$sBasketItem.price|currency} {block name='frontend_checkout_cart_tax_symbol'}*{/block}
					{/if}
				</strong>
			</div>
			{/block}
		</div>
		<div class="clear">&nbsp;</div>
	</form>
</div>

{* Selected premium article *}
{elseif $sBasketItem.modus == 1}

<div class="cart-item selected_premium ">

		<div class="basketitemblock-a">
			<div class="item-block item-block-1">
				<div class="block-1-top">
					<div class="cartimg-wrap">
						{block name='frontend_checkout_cart_item_premium_image'}
							<span class="premium_img">
								<span class="title">{se name="sCartItemFree"}GRATIS!{/se}</span>
							</span>
						{/block}
					</div>
					<div class="cart-item-title-wrap">
						{block name='frontend_checkout_cart_item_premium_details'}
						<div class="basket_details">
							{$sBasketItem.articlename}
							<!-- ARTICLE NAME, ORDERNUMBER, DELIVERY INFO -->

						</div>
						{/block}
					</div>
				</div>
				<div class="clear"></div>
				<div class="block-1-bottom">
						
					{s name="CartItemInfoPremium"}{/s}

				</div>
			</div>
		</div>
		<div class="basketitemblock-b">

			<div class="item-block item-block-2">
				<!-- QUANTITY + PRICE PER PIECE -->
			</div>


			<div class="item-block item-block-3">
				{block name='frontend_checkout_cart_item_premium_tax_price'}{/block}
			</div>

			{block name='frontend_checkout_cart_item_premium_price'}
			<div class="item-block item-block-4">
				<strong>
					{s name="CartItemInfoFree"}{/s}
				</strong>
			</div>
			{/block}
		</div>
		{block name='frontend_checkout_cart_item_premium_delete'}
			<div class="action">
				{if $sBasketItem.modus == 0}
					<a href="{url action='deleteArticle' sDelete=$sBasketItem.id sTargetAction=$sTargetAction}" class="del" title="{s name='CartItemLinkDelete'}{/s}">&nbsp;</a>
				{/if}
			</div>
		{/block}

		<div class="clear">&nbsp;</div>
	</form>
</div>

{* Extra charge for small quantities *}
{elseif $sBasketItem.modus == 4}
<div class="cart-item small_quantities">
		{block name='frontend_checkout_cart_item_small_quantities_details'}
		<div class="basketitemblock-a">
			<div class="item-block item-block-1">
				<div class="block-1-top">
					<div class="cartimg-wrap"></div>
					<div class="cart-item-title-wrap">

						<div class="basket_details">
							<span class="title">{$sBasketItem.articlename}</span>
						</div>
					</div>
				</div>
				<div class="clear"></div>
				<div class="block-1-bottom">
						
						<!-- ARTICLE DETAILS ( ATTRIBUTES / DESCRIPTION) -->

				</div>
			</div>
		</div>
		{/block}
		<div class="basketitemblock-b">

			<div class="item-block item-block-2">
			</div>


			<div class="item-block item-block-3">
				{block name='frontend_checkout_cart_item_small_quantites_tax_price'}{/block}
			</div>

			<div class="item-block item-block-4">
				<strong>
					{if $sBasketItem.itemInfo}
						{$sBasketItem.itemInfo}
					{else}
						{$sBasketItem.price|currency} {block name='frontend_checkout_cart_tax_symbol'}*{/block}
					{/if}
				</strong>
			</div>

		</div>
		<div class="clear">&nbsp;</div>
	</form>
</div>


	
{* Bundle discount price *}
{elseif $sBasketItem.modus == 10}

<div class="cart-item bundle_row">

		<div class="basketitemblock-a">
			<div class="item-block item-block-1">
				<div class="block-1-top">
					<div class="cartimg-wrap">
					</div>
					<div class="cart-item-title-wrap">

						<div class="basket_details">

							<span class="title">{s name='CartItemInfoBundle'}{/s}</span>
							
						</div>
					</div>
				</div>
				<div class="clear"></div>
				<div class="block-1-bottom">
				</div>
			</div>
		</div>
		<div class="basketitemblock-b">

			<div class="item-block item-block-2">
			</div>


			<div class="item-block item-block-3">
				{block name='frontend_checkout_cart_item_bundle_tax_price'}{/block}
			</div>
			{block name='frontend_checkout_cart_item_bundle_price'}
			<div class="item-block item-block-4">
				<strong>
					{$sBasketItem.amount|currency} {block name='frontend_checkout_cart_tax_symbol'}*{/block}
				</strong>
			</div>
			{/block}
		</div>
		<div class="clear">&nbsp;</div>
	</form>
</div>
{/if}