{extends file='parent:frontend/checkout/cart_header.tpl'}
{block name='frontend_checkout_cart_header_field_labels'}
	{* Article informations *}
	{block name='frontend_checkout_cart_header_name'}
		<div class="basketheader-blocka">
			<div class="basketHeaderCol basketheader-col1">
				{s name="CartColumnName" namespace="frontend/checkout/cart_header"}{/s}
			</div>
		</div>
	{/block}
	
	{* Delivery informations *}
	{block name='frontend_checkout_cart_header_availability'}{/block}
	
	{block name='frontend_checkout_table_blockb'}
		<div class="basketheader-blockb">
			{* Article amount *}
			{block name='frontend_checkout_cart_header_quantity'}
				<div class="basketHeaderCol basketheader-col2">
					{s name="CartColumnQuantity" namespace="frontend/checkout/cart_header"}{/s}
				</div>
			{/block}
			
			{* Unit price *}
			{block name='frontend_checkout_cart_header_price'}{/block}
			
			{* Article tax *}
			{block name='frontend_checkout_cart_header_tax'}
				<div class="basketHeaderCol charge_vat basketheader-col3">
				{if $sUserData.additional.charge_vat && !$sUserData.additional.show_net}
					{se name='CheckoutColumnExcludeTax' namespace="frontend/checkout/confirm_header"}{/se}
				{elseif $sUserData.additional.charge_vat}
					{se name='CheckoutColumnTax' namespace="frontend/checkout/confirm_header"}{/se}
				{else}&nbsp;{/if}
				</div>
			{/block}
			
			{* Article total sum *}
			{block name='frontend_checkout_cart_header_total'}
				<div class="basketHeaderCol basketheader-col4">
					<span>{s name="CartColumnTotal"}{/s}</span>
				</div>
			{/block}
		</div>
	{/block}
	<div class="clear"></div>
{/block}