{extends file='parent:frontend/index/header.tpl'}

{block name="frontend_index_header_css_screen" append}
	<link type="text/css" media="screen, projection" rel="stylesheet" href="{link file='frontend/_resources/flexslider/flexslider.css'}"/>
	<link type="text/css" media="screen, projection" rel="stylesheet" href="{link file='frontend/_resources/style/t2re.css'}"/>
{/block}

{block name="frontend_index_header_javascript_jquery" append}
	<script type="text/javascript" src="{link file='frontend/_resources/js/t2re.js'}"></script>
	<script type="text/javascript" src="{link file='frontend/_resources/flexslider/jquery.flexslider-min.js'}"></script>
{/block}
