<?php
/**
 * Fabelhaft Template 1.0.6
 * Copyright © 2014-2015 shop-templates
 *
 * @category   Shopware
 * @package    Shopware_Plugins
 * @subpackage Plugin
 * @copyright  Copyright (c) 2014-2015, shop-templates.com
 * @version    $Id$
 * @author     shop-templates
 */
class Shopware_Plugins_Frontend_StcomTemplateFabelhaft_Bootstrap extends Shopware_Components_Plugin_Bootstrap
{
	
    /**
     * Installs the template plugin
     *
     * @public
     * @return bool
     */
    public function install()
    {
    	$this->checkLicense();
		$this->createConfiguration();
        $this->subscribeEvents();
        $this->createTemplate($this->getTemplateMetaInfo());
        return true;
    }
 
    /**
     * Gets the version of the plugin.
     *
     * @public
     * @return string
     */
    public function getVersion() {
	    return '1.0.6';
    }

	/**
     * Provides the template meta information.
     *
     * @public
     * @return array
     */
    public function getTemplateMetaInfo() {
	    return array(
            'name' => 'Fabelhaft',
            'template' => 'emotion_fabelhaft',
            'version' => 2,
            'description' => null,
            'author' => 'shop-templates.com',
            'license' => '(c) shop-templates.com, 2014',
            'esi' => true,
            'emotion' => true
        );
    }

 
    /**
     * Creates and subscribe the events and hooks.
     *
     * @protected
     * @return void
     */
    protected function subscribeEvents()
    {
        $this->subscribeEvent(
            'Enlight_Template_Manager_ResolveTemplateDir',
            'onResolveTemplateDir'
        );
    	$this->subscribeEvent(
            'Enlight_Controller_Action_PostDispatch', 
            'onPostDispatch'
        );
		$this->subscribeEvent(
            'Enlight_Controller_Dispatcher_ControllerPath_Widgets_StcomFabelhaft',
            'onGetWidgetController'
        );
		$this->subscribeEvent(
		  'Shopware_Controllers_Widgets_Emotion_AddElement',
		  'onAddElement'
		);
    }
	
	/**
     * Return plugin's capabilities
     *
     * @public
     * @return array
     */
	public function getCapabilities()
		{
		    return array(
		        'install' => true,
		        'update' => true,
		        'enable' => true
		    );
		}
    /**
     * Returns the path to a frontend controller for an event.
     *
     * @public
     * @param Enlight_Event_EventArgs $args
     * @return string
     */
    public function onResolveTemplateDir(Enlight_Event_EventArgs $args)
    {
        $return = $args->getReturn();
        $tplMeta = $this->getTemplateMetaInfo();
        
        if($return === $tplMeta['template']) {
            return $this->Path() . 'Views/';
        }
        return $return;
    }
	/**
     * Creates a new configuration to allow the user to update the grid
     *
     * @private
     * @return void
     */
	private function createConfiguration() {
		$form = $this->Form();
		
		$form->setElement('checkbox', 'updateEmotion', 
	        array(
	            'label' => 'Einkaufswelten anpassen', 
	            'value' => NULL,
	            'description' => 'Passt die Breite der Einkaufwelten für das Fabelhaft Template an. '
	        )
	    );

		$form->setElement('mediaselection', 'logoretina', 
	        array(
	            'label' => 'Logo (für hochauflösende Displays) (640x360px)', 
	            'value' => NULL
	        )
	    );
		$form->setElement('mediaselection', 'logo', 
	        array(
	            'label' => 'Logo (320x180px)', 
	            'value' => NULL
	        )
	    );
	}

	/**
     * Post Dispatch Frontend Function
     *
     * @public
	 * @param Enlight_Event_EventArgs $arguments
     * @return void
     */
	public function onPostDispatch(Enlight_Event_EventArgs $arguments)
    {
    	/**@var $controller Shopware_Controllers_Frontend_Index*/
        $controller = $arguments->getSubject();
		$request  = $controller->Request();
		$response = $controller->Response();
        $view = $controller->View();
		$temppath = Shopware()->DocPath().'templates/_fabelhaft_local/';
		
		if(!$request->isDispatched()
                || $request->getModuleName() != 'frontend'
                || !$arguments->getSubject()->View()->hasTemplate()
                ) {
                return;
            }


		$template = Shopware()->Shop()->getTemplate()->getName();
        if ($template != "Fabelhaft") {
            return;
        }

        // Register Template Dirs
		Shopware()->Template()->addTemplateDir( $this->Path().'/ResView/', 't2responsive', Enlight_Template_Manager::POSITION_PREPEND );

		Shopware()->Template()->addTemplateDir( $temppath, 'fabelhaft_local', Enlight_Template_Manager::POSITION_PREPEND );

		// Add Template Vars
		$config = $this->config();

		$view->assign('fabelhaft', $config);
   }
	/**
	 * Registers the widget controller
	 * 
	 * @public
	 * @param Enlight_Event_EventArgs $arguments
	 * @return void
	 */
	public function onGetWidgetController(Enlight_Event_EventArgs $arguments) {
        return $this->Path(). 'Controllers/Widgets/StcomFabelhaft.php';
	}
	/**
     * This function will update the plugin... this is the first public version so it does nothing, yet
     *
     * @public
	 * @param String $oldVersion
     * @return boolean
     */
	public function update($oldVersion) {

		return true;
	}

	/**
     * Return the label
     *
     * @public
     * @return string
     */
	public function getLabel()
	{
	    return 'Fabelhaft';
	}
    /**
     * Gets the plugin's info 
     *
     * @public
     * @return array
     */
    public function getInfo()
    {        
        return array(
            'version' => $this->getVersion(),
            'autor' => 'shop-templates.com',
            'copyright' => 'Copyright © 2014 shop-templates.com',
            'label' => $this->getLabel(),
            'source' => "Local",
            'description' => '<img src="http://images.shop-templates.com/shop-templateslogo.png" alt="shop-templates.com" style="float: right; margin: 0px 0px 10px 15px;">
            <h1></h1>
            <p><table>
                <tr><td style="width: 55px;"><strong>E-Mail: </strong></td><td> <a href="mailto:support@shop-templates.com" target="_blank">support@shop-templates.com</a></td></tr>
                <tr><td><strong>Website: </strong></td><td> <a href="http://www.shop-templates.com" target="_blank"> www.shop-templates.com</a></td></tr>
            </table></p><br>
            <p><b>shop-templates.com</b> ist das Portal für hochwertige Designer Templates und Plugins. Ganz einfach lässt sich auf diese Art und Weise Ihr Shopware Shop auf Ihre Wünsche erweitern.<br />
                <br />
                Jedes unserer Templates & Plugins kann nach Ihren Wünschen individualisiert werden.<br />
                <br />
                Bei Fragen zur Individualisierung oder Erweiterung schreiben Sie uns einfach eine Mail an:<br />
                <a href=”mailto:individual@shop-templates.com”>individual@shop-templates.com</a><br />
                <br />
                Haben Sie Fragen zu unseren Produkten, dann besuchen Sie unsere <a href=”http://www.shop-templates.com/faq-soforthilfe”>FAQ & Soforthilfe</a> Seite. Dort finden Sie Antworten auf die häufigsten Fragen zu unseren Produkten. </p>',
                'license' => 'shop-templates.com',
                'support' => 'http://www.shop-templates.com/',
                'link' => 'http://www.shop-templates.com/'
                );        
} 
	/**
     * Hooks the onAddElement function of widgets/Emotion.php and adds topseller + newcomer articles for article slider elements.
     *
     * @public
	 * @param Enlight_Event_EventArgs $args
     * @return array
     */
	public function onAddElement(Enlight_Event_EventArgs $args) {
        $element = $args->getElement();
        $data = $args->getReturn();    
        $request = $args->getSubject()->Request(); 

        $categoryId = (int) $request->getParam('sCategory');
        if (empty($categoryId)) {
            $categoryId = (int) Shopware()->Shop()->get('parentID');
        }
  		$maxArticles = $data['article_slider_max_number'];
		if (!is_numeric($maxArticles)) { $maxArticles = 16; }
        if ($element["component"]["name"]=="Artikel-Slider"){
        	if ($data['article_slider_type'] == "topseller") {
        		$topseller = $this->getProductTopSeller($categoryId,0,$maxArticles);
   				$data['topsellerArticles'] = $topseller['values'];
        	}   
			if ($data['article_slider_type'] == "newcomer") {
        		$newcomer = $this->getProductNewcomer($categoryId,0,$maxArticles);
   				$data['newcomerArticles'] = $newcomer['values'];
        	}              
   			
        }
		return $data;
	}
	/**
     * Get topseller articles from a specific category
     *
     * @private
	 * @param Integer $category 
	 * @param Integer $offset 
	 * @param Integer $limit
     * @return array
     */
    private function getProductTopSeller($category, $offset = 0, $limit) {
        $sql = "
            SELECT
              STRAIGHT_JOIN
              SQL_CALC_FOUND_ROWS

              a.id AS articleID,
              s.sales AS quantity

            FROM s_articles_top_seller_ro s

            INNER JOIN s_articles_categories_ro ac
              ON ac.articleID = s.article_id
              AND ac.categoryID = :categoryId

            INNER JOIN s_categories c
              ON ac.categoryID = c.id
              AND c.active = 1

            INNER JOIN s_articles a
              ON a.id = s.article_id
              AND a.active = 1

            GROUP BY a.id
            ORDER BY quantity DESC
        ";

        $sql = Shopware()->Db()->limit($sql, $limit, $offset);
        $articles = Shopware()->Db()->fetchAll($sql, array('categoryId' => $category));

        $count = Shopware()->Db()->fetchOne("SELECT FOUND_ROWS()");
        $pages = round($count / $limit);

        if ($pages == 0 && $count > 0) {
            $pages = 1;
        }

        $values = array();

        foreach ($articles as &$article) {
            $articleId = $article["articleID"];

            $value = Shopware()->Modules()->Articles()->sGetPromotionById('fix', 0, $articleId, false);
            if (!$value) {
                continue;
            }
            $values[] = $value;
        }

        return array("values" => $values, "pages" => $pages);
    }
    /**
     * Get topseller articles from a specific category
     *
     * @private
	 * @param Integer $category 
	 * @param Integer $offset 
	 * @param Integer $limit
     * @return array
     */
    private function getProductNewcomer($category, $offset = 0, $limit) {
        $sql = "
            SELECT DISTINCT SQL_CALC_FOUND_ROWS a.id AS id
            FROM s_articles a
              INNER JOIN s_articles_categories_ro ac
                 ON ac.articleID = a.id
              INNER JOIN s_categories c
                 ON c.id = ac.categoryID
                 AND c.active = 1

            WHERE a.active=1
            AND c.id=?

            ORDER BY a.datum DESC
        ";

        $sql = Shopware()->Db()->limit($sql, $limit, $offset);

        $articles = Shopware()->Db()->fetchAll($sql, array($category));

        $count = Shopware()->Db()->fetchOne("SELECT FOUND_ROWS()");
        $pages = round($count / $limit);

        $values = array();
        foreach ($articles as &$article) {
            $articleId = $article["id"];

            $value = Shopware()->Modules()->Articles()->sGetPromotionById('fix', 0, $articleId, false);
            if (!$value) {
                continue;
            }

            $values[] = $value;
        }

        return array("values" => $values, "pages" => $pages);

    }


	/**
     * Enable function. Fired when the plugin is enabled 
     *
     * @public
     * @return boolean
     */
	public function enable(){
		$config = $this->config();
		if ($config->updateEmotion == true) {
			Shopware()->Db()->query("UPDATE s_emotion SET container_width = '755' WHERE container_width = '1008'");
			Shopware()->Db()->query("UPDATE s_emotion SET container_width = '755' WHERE container_width = '808'");
		}
		else {
			
			Shopware()->Db()->query("UPDATE s_emotion SET container_width = '808' WHERE container_width = '755'");
		}
		return true;
		
	}
	/**
     * Uninstall function. Restores all changed storefront sizes
     *
     * @public
     * @return boolean
     */
	public function uninstall() {

		return true;
	}

	/**
     * Checks if the user has a valid license
     *
     * @public
	 * @param 
     * @return boolean
     */	
    public function checkLicense($throwException = true)
    {
      try {
        static $r, $module = 'StcomTemplateFabelhaft';
        if(!isset($r)) {
          $s = base64_decode('FVE9D8h8uRHSwqdCuhV10aTpyno=');
          $c = base64_decode('7qHIofbPsj48zosd2pUnDWzAvVs=');
          $r = sha1(uniqid('', true), true);
          /** @var $l Shopware_Components_License */
          $l = $this->Application()->License();
          $i = $l->getLicense($module, $r);
          $t = $l->getCoreLicense();
          $u = strlen($t) === 20 ? sha1($t . $s . $t, true) : 0;
          $r = $i === sha1($c. $u . $r, true);
        }
        if(!$r && $throwException) {
          throw new Exception('License check for module "' . $module . '" has failed.');
        }
        return $r;
      } catch (Exception $e) {
        if($throwException) {
          throw new Exception('License check for module "' . $module . '" has failed.');
        } else {
          return false;
        }
      }
    } 
}