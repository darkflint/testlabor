{* Footer menu *}
<div class="footer_menu">
	
	<div class="footer_column col1">
		{if $fabelhaft.logo}
			<img src="{link file={$fabelhaft.logo}}" alt="" class="" />
		{else}
			<img src="{link file='frontend/_resources/images/logo_footer.png'}" alt="" class="" />
		{/if}
		<div class="socialicons">
			<a href="{s name='SocialLinkGooglePlus'}#{/s}" target="_blank"><i class="fa fa-google-plus fa-2x"></i></a>
			<a href="{s name='SocialLinkTwitter'}#{/s}" target="_blank"><i class="fa fa-twitter fa-2x"></i></a>
			<a href="{s name='SocialLinkFacebook'}#{/s}" target="_blank"><i class="fa fa-facebook fa-2x"></i></a>
		</div>
	</div>
	
	<div class="footer_column col2">
		<span class="head">{s name="sFooterShopNavi2"}Informationen{/s}</span>
		<ul>
		{foreach from=$sMenu.gBottom2 item=item key=key name="counter"}
			<li>
				<a href="{if $item.link}{$item.link}{else}{url controller='custom' sCustom=$item.id title=$item.description}{/if}" title="{$item.description}" {if $item.target}target="{$item.target}"{/if}>
					{$item.description}
				</a>
			</li>
		{/foreach}
		</ul>
	</div>
	<div class="footer_column col3">

		<span class="head">{s name="sFooterShopNavi1"}Kundenservice{/s}</span>
		<ul>
		{foreach from=$sMenu.gBottom item=item  key=key name="counter"}
			<li>
				<a href="{if $item.link}{$item.link}{else}{url controller='custom' sCustom=$item.id title=$item.description}{/if}" title="{$item.description}" {if $item.target}target="{$item.target}"{/if}>
					{$item.description}
				</a>
			</li>
		{/foreach}
		</ul>
	</div>
	<div class="footer_column col4 last">
		<span class="head">{s name="sFooterPaymentHead"}Zahlungsmöglichkeit{/s}</span>
		<img src="{link file='frontend/_resources/images/payment.png'}" alt="" class="" />
	</div>
		<div class="taxinfo">
						        {if $sOutputNet}
									<p>{s name='FooterInfoExcludeVat' namespace="frontend/index/footer"}&nbsp;{/s}</p>
								{else}
									<p>{s name='FooterInfoIncludeVat' namespace="frontend/index/footer"}&nbsp;{/s}</p>
								{/if}
						</div>	
</div>
