{extends file="parent:frontend/detail/index.tpl"}

{block name="frontend_index_header_javascript_jquery"}
    {block name="frontend_index_header_javascript_jquery_promotion"}
        {$smarty.block.parent}
        <script type="text/javascript">
            (function ($) {

                $.subscribe('plugin/swOffcanvasMenu/onCloseMenu', function () {
                    var plugin = $('.free_goods-product--selection').data('plugin_promotionFreeGoodsSlider');
                    if (plugin !== undefined) {
                        plugin.destroy();
                    }
                });

            })(jQuery);
        </script>
    {/block}
{/block}

{block name="frontend_detail_index_actions"}
    {$smarty.block.parent}
    {block name="frontend_detail_index_actions_promotion"}
        {include file="frontend/swag_promotion/detail/actions.tpl"}
    {/block}
{/block}
