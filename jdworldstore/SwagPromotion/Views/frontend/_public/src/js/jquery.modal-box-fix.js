;(function($, window) {
    'use strict';

    var offset = 20;

    $.modal.setHeight = function (height) {
        var me = this;

        window.setTimeout(function() {
            var hasTitle = me._$title.text().length > 0,
                headerHeight;

            if(hasTitle) {
                headerHeight = window.parseInt(me._$header.css('height'), 10);
                headerHeight += offset;

                me._$content.css('height', (typeof height === 'string' && !(/^\d+$/.test(height))) ? (height - headerHeight) : (window.parseInt(height, 10) - headerHeight));
            } else {
                me._$content.css('height', '100%');
            }
        }, 0);

        me._$modalBox.css('height', (typeof height === 'string' && !(/^\d+$/.test(height))) ? height : window.parseInt(height, 10));
        $.publish('plugin/swModal/onSetHeight', [ me ]);
    };
})(jQuery, window);