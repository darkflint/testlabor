{extends file="parent:frontend/checkout/ajax_cart.tpl"}

{block name='frontend_checkout_ajax_cart_alert_box'}
    {$smarty.block.parent}
    {block name="frontend_checkout_ajax_cart_alert_box_used_promotions"}
        {if $promotionsUsedTooOften}
            {include file="frontend/swag_promotion/checkout/used_too_often_offcanvas.tpl"}
        {/if}
    {/block}
    {if !empty($freeGoods)}
        {block name="frontend_checkout_ajax_cart_alert_box_promotion"}
            {include file="frontend/swag_promotion/checkout/free_goods.tpl"}
        {/block}
    {/if}
{/block}
