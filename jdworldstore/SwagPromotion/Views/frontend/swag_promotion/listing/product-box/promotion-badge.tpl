{$promotionStruct = $sArticle.attributes.promotion}
{if !empty($promotionStruct->promotions)}
    {$description = {eval var=$promotionStruct->promotions.0->description}}
    <div class="product--badge badge--recommend promotionBadge" title="{$description|strip_tags}">
        {s name="promotionBadge" namespace="frontend/swag_promotion/main"}{/s}
    </div>
{/if}
