{**
 * Shopware 3.5 Template
 *
 * @category   Shopware
 * @package    Shopware_Template
 * @subpackage Shopware_Template_Frontend
 * @copyright  Copyright (c) 2010 shopware AG (http://www.shopware.de)
 * @author     hl/shopware AG
 * @author     stp/shopware AG
 *}
{block name="frontend_index_start"}{/block}
<?xml version="1.0" ?>
{block name="frontend_index_doctype"}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
{/block}
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de">
{block name='frontend_index_header'}
{include file='./frontend/index/header.tpl'}
{/block}
<body {if $Controller}class="ctl_{$Controller}"{/if}>
<div class="wrapEverything">
	<div class="wrapEverythingOverlay"></div>

{* Message if javascript is disabled *}
{block name="frontend_index_no_script_message"}
<noscript>
	<div class="notice bold center noscript_notice">
		{s name="IndexNoscriptNotice"}{/s}
	</div>
</noscript>
{/block}

{block name='frontend_index_before_page'}
{/block}

<div id="top"></div>
<div id="dl-menu" class="dl-menuwrapper">
				{include file="frontend/plugins/t2mobilemenu/t2mobilemenu.tpl"}
			</div>
{* Shop header *}
{block name='frontend_index_navigation'}
	<div id="mobileTabBar">
		<div class="tabs">
			<div class="tab t2mmButtonShow dl-trigger">
				<span class="icon flaticon-list26"></span><br>
				<span class="title">Menü</span>
			</div>
			<a href="{url controller='account'}" title="{s namespace='frontend/index/checkout_actions' name='IndexLinkAccount'}{/s}" class="tab account">
				<span class="icon flaticon-mustache"></span><br>
				<span class="title">{s namespace='frontend/index/checkout_actions' name='IndexLinkAccount'}{/s}</span>
			</a>
			<a href="{url controller='note'}" title="{s namespace='frontend/index/checkout_actions' name='IndexLinkNotepad'}{/s}" class="tab">
				<span class="icon flaticon-notepad3"></span><br>
				<span class="title">Merkzettel</span>
			</a>
		</div>
		<a class="mBasket" href="{url controller='checkout' action='cart'}" title="{s namespace='frontend/index/checkout_actions' name='IndexLinkCart'}{/s}">
			{action module=widgets controller=StcomFabelhaft action=mobileBasket}
		</a>

		
		
	</div>
	<div id="header">
		<div class="inner">
			<div class="inner-bg">
			</div>
			{* Trusted Shops *}
			{if {config name=TSID}}
			<div class="trusted_shops_top">
				<a href="https://www.trustedshops.com/shop/certificate.php?shop_id={config name=TSID}" title="{s name='WidgetsTrustedLogo' namespace='frontend/plugins/trusted_shops/logo'}{/s}" target="_blank">
				    <img src="{link file='frontend/_resources/images/logo_trusted_shop_top_big.png'}" alt="{s name='WidgetsTrustedLogo' namespace='frontend/plugins/trusted_shops/logo'}{/s}" />
				</a>
			</div>
			{/if}
			
	
		
			{* Language and Currency bar *}
			{block name='frontend_index_actions'}{/block}
			
			{* Shop logo *}
			{block name='frontend_index_logo'}
			<div id="logo" class="grid_5">
				<a href="{url controller='index'}" title="{$sShopname} - {s name='IndexLinkDefault'}{/s}">{$sShopname}</a>
			</div>
			{/block}
			{* Search *}
            {block name='frontend_index_search'}
			    {include file="frontend/index/search.tpl"}
            {/block}

            
			{* Shop navigation *}
			{block name='frontend_index_checkout_actions'}
				{block name="frontend_index_checkout_actions_my_options"}
				<div class="my_options">
				
						<ul>
						{foreach from=$sMenu.gLeft item=item key=key name="counter"}
							<li {if $smarty.foreach.counter.last} class="last" {/if} >
								<a href="{if $item.link}{$item.link}{else}{url controller='custom' sCustom=$item.id title=$item.description}{/if}" title="{$item.description}" {if $item.target}target="{$item.target}"{/if}>
									{$item.description}
								</a>
							</li>
						{/foreach}
						</ul>
				
				
				        {block name="frontend_index_checkout_actions_service_menu"}
				        {/block}
				
						{* Language and Currency bar *}
				        {block name='frontend_index_actions'}
				            {action module=widgets controller=index action=shopMenu}
				        {/block}
				    <div class="clear">&nbsp;</div>
				</div>
				{/block}
				{action module=widgets controller=checkout action=info}
			{/block}
			
			{block name='frontend_index_navigation_inline'}
				{if $sCompareShow}
				<div id="compareContainerAjax">
				    {action module=widgets controller=compare}
				</div>
				{/if}
			{/block}
		

		</div>
	</div>
	
	<div id="wrapper">
			<div class="wrap_inner">
				<div class="mainNavigation_wrap">
					
					{* Maincategories navigation top *}
					{block name='frontend_index_navigation_categories_top'}
						{include file='frontend/index/categories_top.tpl'}
					{/block}
				</div>
			
{/block}

			<div class="container_20">				
				
				{* Content section *}
				<div id="content">
					<div class="inner">
						
						{* Content top container *}
						{block name="frontend_index_content_top"}{/block}
						
						{* Sidebar left *}
						{block name='frontend_index_content_left'}
							{include file='frontend/index/left.tpl'}
						{/block}
						
						
						{* Main content *}
						{block name='frontend_index_content'}
							{* Breadcrumb *}
							{block name='frontend_index_breadcrumb'}
								{include file='frontend/index/breadcrumb.tpl'}
							{/block}
						{/block}
						
						
						{* Sidebar right *}
						{block name='frontend_index_content_right'}{/block}
						
						<div class="clear">&nbsp;</div>
						
					</div>
				</div>
				{* Footer REMOVED *}
				{block name="frontend_index_footer"}
				{/block}
			</div>
			
		{block name="frontend_index_shopware_footer"}
		</div>
	</div>

	{* FOOTER *}
	
	<div id="footer_wrapper">
		<div class="footer_inner">
			<div class="clear"></div>
			{include file='frontend/index/footer.tpl'}
		</div>
		
		<div class="clear"></div>

	</div>
	{/block}
{block name='frontend_index_body_inline'}{/block}
</div>
</body>
</html>

