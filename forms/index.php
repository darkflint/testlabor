<!DOCTYPE html>
<html lang="de">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <meta name="description" content="" />
        <meta name="format-detection" content="telephone=no" />
        <title></title>
        <link rel="stylesheet" href="lib/bootstrap-3.3.6-dist/css/bootstrap.min.css" />
        <link rel="stylesheet" href="lib/jQuery-Form-Validator-master/form-validator/theme-default.min.css" />
        <style>
            form {
                width: 50%;
                margin: 0 auto;
            }
            
            label {
                margin: 0;
                vertical-align: middle;
            }
            
            input[type="radio"], input[type="checkbox"] {
                display: none;
            }
            
            input[type="radio"] + .radio span {
                width: .5em;
                height: .5em;
                border-radius: 50%;
                display: inline-block;
                position: relative;
                top: -5px;
            }
            
            input[type="radio"]:checked + .radio span {
                background-color: black;
            }
            
            input[type="checkbox"] + .checkbox {
                background-color: grey;
            }
            
            input[type="checkbox"] + .checkbox .glyphicon {
                display: none;
            }
            
            input[type="checkbox"]:checked + .checkbox {
                background-color: white;
            }
            
            input[type="checkbox"]:checked + .checkbox .glyphicon {
                display: inline-block;
            }
            
            .radio, .checkbox {
                height: 1em;
                width: 1em;
                border: 1px solid black;
                display: inline-block;
                cursor: pointer;
                text-align: center;
                margin: 0;
                vertical-align: middle;
            }
            
            .radio {
                border-radius: 50%;
            }
            
            .checkbox .glyphicon {
                font-size: .8em;
                top: -3px;
            }
        </style>
    </head>
    <body>
        <header>
            <a href="../index.html">Back</a>
        </header>
        
        <form name="test_form">
            <fieldset>
                <legend>Style Radio</legend>
                
                <div>
                    <label for="radio1">Radio1</label>
                    <input type="radio" name="radio" id="radio1" />
                    <div class="radio"><span></span></div>
                </div>
                
                <div>
                    <label for="radio2">Radio2</label>
                    <input type="radio" name="radio" id="radio2" />
                    <div class="radio"><span></span></div>
                </div>
                
                <div>
                    <label for="radio3">Radio3</label>
                    <input type="radio" name="radio" id="radio3" />
                    <div class="radio"><span></span></div>
                </div>
            </fieldset>
            
            <fieldset>
                <legend>Style Check</legend>
                
                <div>
                    <label for="check">Checkbox</label>
                    <input type="checkbox" name="check" id="check" />
                    <div class="checkbox"><span class="glyphicon glyphicon-ok"></span></div>
                </div>
            </fieldset>
            
            <fieldset>
                <legend>Validation</legend>
                
                <div id="email-error-dialog"></div>
                
                <p>
                    User name (4 characters minimum, only alphanumeric characters):
                    <input data-validation="length alphanumeric" data-validation-length="min4">
                </p>
                
                <p>
                    Year (yyyy-mm-dd):
                    <input data-validation="date" data-validation-format="yyyy-mm-dd">
                </p>
                
                <p>
                    Website:
                    <input data-validation="url">
                </p>
                
                <div class="form-group">
                    <label class="control-label" for="the-input">Input description</label>
                    <input type="text" id="the-input" class="form-control"  
                         data-validation-error-msg-container="#email-error-dialog" />
                </div>
                
                
                <p>
                    E-mail:
                    <input name="user-email" data-validation="email"  type="email"
                         data-validation-error-msg="You did not enter a valid e-mail" 
                         data-validation-error-msg-container="#email-error-dialog">
                </p>
                
                <button type="submit">Absenden</button>
            </fieldset>
        </form>
        
        <script src="lib/jquery-2.2.0.min.js"></script>
        <script src="lib/jQuery-Form-Validator-master/form-validator/jquery.form-validator.min.js"></script>
        <script>
            jQuery(document).ready(function($) {
                $(document).on("click", ".radio, .checkbox", function() {
                    var checkbox = $(this).siblings("input");
                    var checked = checkbox.prop("checked");
                    checkbox[0].checked = !checked;
                });
                
                $.validate({
                    validateOnBlur : false, // disable validation when input looses focus
                    errorMessagePosition : 'top', // Instead of 'element' which is default
                    scrollToTopOnError : false // Set this property to true if you have a long form 
                });
            });
        </script>
    </body>
</html>
