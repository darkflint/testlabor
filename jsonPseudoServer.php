<?php

    if (isset($_GET['valid'])){
        switch ($_GET['valid']){
            case 'true':
                $returnValue = '{
                                  "d": {
                                    "results": [
                                      {
                                        "__metadata": {
                                          "id": "http://sap1-vajeezi.business.jungheinrich.com:8000/sap/opu/odata/sap/ZMERGE_PREOP_CHECK_SRV/EquipmentSet(\'48000152Z\')",
                                          "uri": "http://sap1-vajeezi.business.jungheinrich.com:8000/sap/opu/odata/sap/ZMERGE_PREOP_CHECK_SRV/EquipmentSet(\'48000152Z\')",
                                          "type": "ZMERGE_PREOP_CHECK_SRV.Equipment"
                                        },
                                        "ShipToPartyId": "11800208",
                                        "EquiNo": "48000152Z",
                                        "EquiDescr": "EJC 12",
                                        "CustEquiDescr": "4711TEST",
                                        "ProductHierarchy": "13152",
                                        "ManufacturerName": "Jungheinrich-HJC"
                                      },
                                      {
                                        "__metadata": {
                                          "id": "http://sap1-vajeezi.business.jungheinrich.com:8000/sap/opu/odata/sap/ZMERGE_PREOP_CHECK_SRV/EquipmentSet(\'48000152B\')",
                                          "uri": "http://sap1-vajeezi.business.jungheinrich.com:8000/sap/opu/odata/sap/ZMERGE_PREOP_CHECK_SRV/EquipmentSet(\'48000152B\')",
                                          "type": "ZMERGE_PREOP_CHECK_SRV.Equipment"
                                        },
                                        "ShipToPartyId": "11800208",
                                        "EquiNo": "48000152B",
                                        "EquiDescr": "EJC 12",
                                        "CustEquiDescr": "4712TEST",
                                        "ProductHierarchy": "13152",
                                        "ManufacturerName": "Jungheinrich-HJC"
                                      }
                                    ]
                                  }
                                }';
            break;

            case 'false':
                $returnValue = 'simple string value to emulate invalid return value..';
            break;
        }
    }   else {
        $returnValue = false;
    }

    echo $returnValue;

?>