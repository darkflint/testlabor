(function(angular) {
    var modul = angular.module("testLaborApp", [
        "ngRoute",
        "pascalprecht.translate"
    ]);
    
    modul.run(function() {
        console.log("run");
    });
    
    modul.config(["$routeProvider", "$translateProvider",
        function($routeProvider, $translateProvider) {
            var defaultLang = "de_DE";
            
            /**
             * load translation asynchron from files
             */
            $translateProvider.useStaticFilesLoader({
                prefix: "/angular/languages/",
                suffix: ".json"
            });
            
            /**
             * set default language
             */
            $translateProvider.preferredLanguage(defaultLang);
            
            /**
             * sanitize the translation
             */
            $translateProvider.useSanitizeValueStrategy("sanitizeParameters");
            
            /**
             * router
             */
            $routeProvider.when("/", {
                templateUrl: "/angular/views/index.html",
                controller: "indexController"
            }).when("/abc", {
                templateUrl: "/angular/views/abc.html",
                controller: "abcController"
            }).when("/123", {
                templateUrl: "/angular/views/123.html",
                controller: "123Controller"
            }).when("/404", {
                templateUrl: "/angular/views/123.html",
                controller: "123Controller"
            }).otherwise({
                redirectTo: "/404"
            });
            
//            $locationProvider.html5Mode(true);
        }
    ]);
    
    modul.controller("bodyController", ["$scope",
        function($scope) {
            console.log("body");
        }
    ]);
    
    modul.controller("indexController", ["$scope",
        function($scope) {
            console.log("index");
        }
    ]);
    
    modul.controller("abcController", ["$scope",
        function($scope) {
            console.log("abc");
            
            $scope.input =  {
                text: "abc",
                name: "Norman"
            };
            
            $scope.$watchCollection("input.text", function(newValue, oldValue) {
                console.log(newValue, oldValue);
                $scope.input.name = newValue;
            });
        }
    ]);
    
    modul.controller("123Controller", ["$scope",
        function($scope) {
            console.log("123");
        }
    ]);
})(window.angular);